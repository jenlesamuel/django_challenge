from django.conf.urls import url
from . import views


app_name = 'contacts'

urlpatterns = [
    url(r'^add/$', views.add, name='add'),
    url(r'^list/$', views.list, name='list'),
    url(r'^success/$', views.success, name='success'),
    url(r'^$', views.index , name='index'),

]

