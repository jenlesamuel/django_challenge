from django.db import models
from .validators import validate_name

# Create your models here.

class Employee(models.Model):
    name = models.CharField(max_length=20, verbose_name='Name', validators=[validate_name])
    email = models.EmailField(max_length=255, verbose_name='Email')