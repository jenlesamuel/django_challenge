from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
import re

def validate_name(value):
    name_pattern = r'^[a-z ]{3,20}$'
    match_object = re.match(name_pattern, value, re.I)

    if not match_object:
        raise ValidationError(
            _('%(field_name)s should contains letters only with a minimum of 3'), code='invalid', params={'field_name': 'Name'}
        )
