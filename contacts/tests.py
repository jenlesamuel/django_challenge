from django.test import TestCase
from django.urls import reverse
from .models import Employee

# Create your tests here.
class ContactsViewsTest(TestCase):

    def test_index_should_display_page_with_list_and_create_links(self):
        create_link_text = "Add Contact"
        list_link_text = "List All Contacts"

        response = self.client.get(reverse("contacts:index"))
        self.assertContains(response, create_link_text)
        self.assertContains(response, list_link_text)

    def test_add_should_display_add_contact_page(self):
        add_contact_text = 'Add Contact'

        response = self.client.get(reverse('contacts:add'))

        self.assertContains(response, add_contact_text)

    def test_list_should_display_list_page(self):
        list_text = 'Contacts List'

        response = self.client.get(reverse('contacts:list'))
        self.assertContains(response, list_text)

    def test_list_should_display_all_records_in_db(self):
        emp1 = Employee(name='emp1', email='emp1@app.com')
        emp1.save()

        emp2 = Employee(name='emp2', email='emp2@app.com')
        emp2.save()

        response = self.client.get(reverse('contacts:list'))

        self.assertContains(response, 'emp1')
        self.assertContains(response, 'emp1@app.com')
        self.assertContains(response, 'emp2')
        self.assertContains(response, 'emp2@app.com')




