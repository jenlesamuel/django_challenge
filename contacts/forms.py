from django import forms
from .models import Employee

class EmployeeForm(forms.ModelForm):
    error_css_class = 'error'

    class Meta:
        model = Employee
        fields = '__all__'
        widgets = {
            'name' : forms.TextInput(attrs={'placeholder':'Enter Full name'}),
            'email': forms.EmailInput(attrs={'placeholder':'Enter Email'})
        }