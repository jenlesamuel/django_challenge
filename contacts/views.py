from django.shortcuts import render, reverse
from django.http import request
from django.http.response import HttpResponse, HttpResponseRedirect
from .forms import EmployeeForm
from .models import Employee

# Create your views here.

def index(request):
    '''
    Displays the home page
    :param request: The Http request object
    :return: Returns a django.http.response.HttpResponse object
    '''
    return render(request, 'contacts/index.html')

def list(request):
    '''
    Handles logic to display a list of all records in the database
    :param request: The Http request object
    :return: Returns a django.http.response.HttpResponse object
    '''

    records = Employee.objects.all()
    return render(request, 'contacts/list.html', {'records': records})


def add(request):
    '''
    Handles logic to display and add a contact
    :param request: The Http request object
    :return: Returns a django.http.response.HttpResponse object
    '''
    if request.method == 'POST':

        form = EmployeeForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('contacts:success'))
    else:
        form = EmployeeForm()

    return render(request, 'contacts/form.html', {'form':form})


def success(request):
    '''
    Display success page after contact is successfully added
    :param request:  The Http Request object
    :return: Returns a django.http.response.HttpResponse Object

    '''
    return render(request, 'contacts/success.html')
